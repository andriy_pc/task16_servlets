package test;

import pizza.*;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(value = "/test",
loadOnStartup = 1)
public class TestServlet extends HttpServlet {

   /* private static PizzaDelivery delivery = new PizzaDelivery();
    static {
        delivery.addPizza(new Pizza());
        delivery.addPizza(new Pizza());
        delivery.addPizza(new Pizza());
        delivery.addPizza(new Pizza());
    }*/

    @Override
    public void doGet(HttpServletRequest req,
                      HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        out.println("<form action = '/task16_servlets-1.0/test' method='POST'>"
                + "ToServer: <input type='text' name='info’><button type='submit'>Send to server</button> </form>");
        // Echo client's request information
        out.println("<p>Request URI: " + req.getRequestURI() + "</p>");
        req.getRequestURL();
        out.println("<p>Cache-control: " + resp.getHeader("Cache-control") + "</p>");
        out.println("<p>Method: " + req.getMethod() + "</p>");
        out.println("</body></html>");
    }

    @Override
    public void doPost(HttpServletRequest req,
                       HttpServletResponse resp) throws IOException {
        /*String paramWidth = request.getParameter("width");
        int width = Integer.parseInt(paramWidth);

        String paramHeight = request.getParameter("height");
        int height = Integer.parseInt(paramHeight);

        long area = width * height;

        PrintWriter writer = response.getWriter();
        writer.println("<html>Area of the rectangle is: " + area + "</html>");
        writer.flush();*/
        PrintWriter out = resp.getWriter();
        out.write("<html><body>LoL</body></html>");
        out.flush();
    }
}
