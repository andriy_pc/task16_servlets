package servlets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pizza.Delivery;
import pizza.DeliveryContainer;
import pizza.DeliveryContainerFactory;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/update/*")
public class UpdateDelivery extends HttpServlet {
    private static Logger log = LogManager.getLogger();

    private DeliveryContainer container =
            DeliveryContainerFactory.getDeliveryContainer();

    private String page;

    @Override
    public void init() {
        log.info("update: init()");
        page = initPage();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html");

        log.info("update: doGet()");
        try {
            PrintWriter out = response.getWriter();
            out.println(page);
        } catch (IOException e) {
            e.printStackTrace();
        }
        page = initPage();
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        //html <form>'s attribute method can't be put
        doPut(request, response);
    }

    @Override
    public void doPut(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html");

        log.info("update: doPut");
        int idToChange = Integer.parseInt(request.getParameter("id"));
        try {
            String newType = request.getParameter("type");
            String newLocation = request.getParameter("location");

            log.info("new type: " + newType + ", new location: " + newLocation);

            Delivery changeable = container.get(idToChange);
            changeable.setPizzaType(newType);
            changeable.setTargetLocation(newLocation);
            container.addDelivery(idToChange, changeable);

            page = initEndPage();
            doGet(request, response);
        } catch (NullPointerException e) {
            for(StackTraceElement ste : e.getStackTrace()) {
                log.fatal(ste);
            }
        }
    }


    private String initPage() {
        log.info("update: initiating page");
        StringBuilder result = new StringBuilder("<html><body><form action='/task16_servlets-1.0/update' method='post'>");
        result.append("<h2>id of delivery  to change: <input type='text' name='id'></h2><br>");
        result.append("<h2>New pizza type: <input type='text' name='type'></h2><br>");
        result.append("<h2>New target location: <input type='text' name='location'></h2><br>");
        result.append("<button type='submit'>change delivery</button><br>");
        result.append("<a href='deliveries'>observe delivery</a></form></body></html>");

        return result.toString();
    }

    private String initEndPage() {
        log.info("initiating ending page");
        StringBuilder result = new StringBuilder("<html><body>You changed delivery; check it now!");
        result.append("<form action = '/task16_servlets-1.0/deliveries' method = 'get'>");
        result.append("<a href='deliveries'>Observe delivery</a></form></body></html>");

        return result.toString();
    }
}
