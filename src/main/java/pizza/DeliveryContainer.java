package pizza;

import java.util.HashMap;
import java.util.Map;

public class DeliveryContainer {
    private Map<Integer, Delivery> deliveryStorage;

    public DeliveryContainer() {
        deliveryStorage = new HashMap<>();
        deliveryStorage.put(1, new Delivery());
        deliveryStorage.put(2, new Delivery());
        deliveryStorage.put(3, new Delivery());
        deliveryStorage.put(4, new Delivery());
    }
    public void addDelivery(int index, Delivery delivery) {
        deliveryStorage.put(index, delivery);
    }
    public Map<Integer, Delivery> getDeliveries() {
        return deliveryStorage;
    }
    public Delivery get(int index) {
        return deliveryStorage.get(index);
    }
    public void remove(int index) {
        deliveryStorage.remove(index);
    }
    public int lastId() {
        return Delivery.getCounter();
    }
}

