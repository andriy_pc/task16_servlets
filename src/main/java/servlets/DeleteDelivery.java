package servlets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pizza.DeliveryContainer;
import pizza.DeliveryContainerFactory;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/delete/*")
public class DeleteDelivery extends HttpServlet {
    private static Logger log = LogManager.getLogger();

    private String page;
    private DeliveryContainer container =
            DeliveryContainerFactory.getDeliveryContainer();

    @Override
    public void init() {
        log.info("Add Delivery init()");
        page = initStartingPage();
    }

    @Override
    public void doGet(HttpServletRequest req,
                      HttpServletResponse resp) {
        resp.setContentType("text/html");
        try {
            PrintWriter out = resp.getWriter();
            out.write(page);
        } catch (IOException e) {
            e.printStackTrace();
        }
        page = initStartingPage();
    }

    @Override
    public void doPost(HttpServletRequest req,
                       HttpServletResponse resp) {
        resp.setContentType("text/html");

        resp.setContentType("text/html");
        String id = req.getParameter("delete-id");
        int index = Integer.parseInt(id);
        log.info("delete delivery with index: " + index);
        container.remove(index);

        page = initEndPage();
        doGet(req, resp);
    }

    private String initStartingPage() {
        log.info("initiating page");
        StringBuilder result = new StringBuilder("<html><body><form action='/task16_servlets-1.0/delete' method='post'>");
        result.append("<h2>id to delete: <input type='text' name='delete-id'></h2><br>");
        result.append("<button type='submit'>Sent delete request</button><br>");
        result.append("</form><a href='deliveries'>Observe delivery</a></body></html>");

        return result.toString();
    }

    private String initEndPage() {
        log.info("initiating ending page");
        StringBuilder result = new StringBuilder("<html><body>You deleted the delivery, check it now!");
        result.append("<a href='deliveries'>Observe delivery</a></body></html>");

        return result.toString();
    }
}
