package servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import pizza.Delivery;
import pizza.DeliveryContainer;
import pizza.DeliveryContainerFactory;

@WebServlet("/deliveries/*")
public class DisplayDeliveries extends HttpServlet {
    private static Logger log = LogManager.getLogger();

    private DeliveryContainer container =
            DeliveryContainerFactory.getDeliveryContainer();

    private static String deliveries;
    private String page;

    @Override
    public void init() {
        log.info("Display delivery init()");
        page = initPage();
    }

    @Override
    public void doGet(HttpServletRequest req,
                      HttpServletResponse resp) {
        resp.setContentType("text/html");

        page = initPage();
        log.info("Display Delivery: doGet()");
        log.info(req.getRequestURI());


        String uri = req.getRequestURI();
        String idUri = uri.replace("/task16_servlets-1.0/deliveries/", "");
        if(!(idUri.equals("/task16_servlets-1.0/deliveries"))) {
            int id = Integer.parseInt(idUri);
            page = initDeliveryPage(id);
        }

        try {
            PrintWriter out = resp.getWriter();
            out.write(page);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String initTable() {
        log.info("Display Delivery: initiating table of deliveries");
        Delivery tmp;
        StringBuilder result = new StringBuilder("<table border=\"1px solid gray\">");
        result.append("<tr><td>#</td><td>Pizza type</td><td>Location</td></tr>");
        for(int i = 1; i <= container.lastId(); ++i) {

            Optional<Delivery> opt = Optional.ofNullable(container.get(i));
            if(opt.isPresent()) {
                tmp = opt.get();
            } else {
                continue;
            }

            result.append("<tr><td>");
            result.append("<a href = 'deliveries/");
            result.append(tmp.getId());
            result.append("'>");
            result.append(tmp.getId());
            result.append("</a>");
            result.append("</td><td>");
            result.append(tmp.getPizzaType());
            result.append("</td><td>");
            result.append(tmp.getTargetLocation());
            result.append("</td></tr>");
        }
        return result.toString();
    }

    private String initPage() {
        deliveries = initTable();
        log.info("Display Delivery: initiating page");
        StringBuilder result =
                new StringBuilder("<html><body>");
        result.append("<h1>deliveries</h1><br>");
        result.append(deliveries);
        result.append("<a href = 'add-delivery'>create delivery</a><br>");
        result.append("<a href = 'update'>update delivery</a><br>");
        result.append("<a href = 'delete'>delete</a><br>");
        result.append("</body></html>");

        return result.toString();
    }

    private String initDeliveryPage(int id) {
        StringBuilder result = new StringBuilder("<html><body>");
        result.append(container.get(id).getPizzaType());
        result.append("<br>");
        result.append(container.get(id).getPizzaType());
        result.append("<a href = '/task16_servlets-1.0/deliveries'>Back to deliveries</a>");
        result.append("</body></html>");

        return result.toString();
    }
}
