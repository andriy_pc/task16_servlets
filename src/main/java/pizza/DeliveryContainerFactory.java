package pizza;

public class DeliveryContainerFactory {
    private static DeliveryContainer instance;

    public static DeliveryContainer getDeliveryContainer() {
        if(instance == null) {
            synchronized (DeliveryContainerFactory.class) {
                if(instance == null) {
                    instance = new DeliveryContainer();
                }
            }
        }
        return instance;
    }
}
