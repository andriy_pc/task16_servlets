package pizza;

public class Delivery {
    private static int counter = 0;
    private int id;
    private String pizzaType;
    private String targetLocation;

    public Delivery() {
        id = ++counter;
        this.pizzaType = "Default type";
        this.targetLocation = "Default location";
    }

    public Delivery(String pizzaType,
                    String targetLocation) {
        id = ++counter;
        this.pizzaType = pizzaType;
        this.targetLocation = targetLocation;
    }

    public String getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }

    public String getTargetLocation() {
        return targetLocation;
    }

    public void setTargetLocation(String targetLocation) {
        this.targetLocation = targetLocation;
    }

    public int getId() {
        return id;
    }

    public static int getCounter() {
        return counter;
    }
}
