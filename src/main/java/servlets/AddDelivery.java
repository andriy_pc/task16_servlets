package servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import pizza.Delivery;
import pizza.DeliveryContainer;
import pizza.DeliveryContainerFactory;

@WebServlet("/add-delivery/*")
public class AddDelivery extends HttpServlet {
    private static Logger log = LogManager.getLogger();

    private String page;
    private DeliveryContainer container =
            DeliveryContainerFactory.getDeliveryContainer();

    @Override
    public void init() {
        log.info("Add Delivery init()");
        page = initStartingPage();
    }

    @Override
    public void doGet(HttpServletRequest req,
                      HttpServletResponse resp) {
        resp.setContentType("text/html");
        log.info("Add Delivery doGet()");
        try {
            PrintWriter out = resp.getWriter();
            out.write(page);
        } catch (IOException e) {
            e.printStackTrace();
        }
        page = initStartingPage();
    }

    @Override
    public void doPost(HttpServletRequest req,
                       HttpServletResponse resp) {
        resp.setContentType("text/html");
        String type = req.getParameter("type");
        log.info("inserted type: " + type);
        String location = req.getParameter("location");
        log.info("inserted location: " + location);
        Delivery newDelivery = new Delivery(type, location);
        log.info("newDelivery id: " + newDelivery.getId());

        page = initEndPage();
        container.addDelivery(newDelivery.getId(), newDelivery);
        doGet(req, resp);
    }

    private String initStartingPage() {
        log.info("Add delivery: initiating page");
        StringBuilder result = new StringBuilder("<html><body><form action='/task16_servlets-1.0/add-delivery' method='post'>");
        result.append("<h2>Pizza type: <input type='text' name='type'></h2><br>");
        result.append("<h2>target location: <input type='text' name='location'></h2><br>");
        result.append("<button type='submit'>Sent delivery request</button><br>");
        result.append("</form><a href='deliveries'>Observe delivery</a></body></html>");

        return result.toString();
    }

    private String initEndPage() {
        log.info("Add delivery: initiating ending page");
        StringBuilder result = new StringBuilder("<html><body>You added new delivery, check it now!");
        result.append("<a href='deliveries'>Observe delivery</a></body></html>");

        return result.toString();
    }

}
